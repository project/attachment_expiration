<?php

/**
 * @file
 * Removes old attachments and their references.
 */

/**
 * Implementation of hook_cron().
 */
function attachment_expiration_cron() {
  if (variable_get('attachment_exp_active', '')) {
    _attachment_expiration_delete();
  }
}

/**
 * Removes attachments older than given date,
 * and removes references to the attachments.
 */
function _attachment_expiration_delete() {
  // Get variable attachments type
  $types = variable_get('attachment_exp_type', '');

  // Convert expiration to timestamp in days ago
  $expiration = strtotime('- ' . variable_get('attachment_exp_date', '') . 'days');

  // Prepend $expiration to $types array
  array_unshift($types, $expiration);

  // Escape user input and build dynamic argument list
  $types_array = db_placeholders($types, 'varchar');

  $sql = "SELECT DISTINCT f.* FROM {upload} u INNER JOIN {files} f ON u.fid = f.fid LEFT JOIN {node} n ON u.nid = n.nid WHERE f.timestamp <= '%s' AND n.type IN ($types_array)";
  $result = db_query(db_rewrite_sql($sql), $types);

  if($result) {
    $files = array();
    while ($file = db_fetch_object($result)) {
      $files[$file->fid] = $file;
    }

    foreach ($files as $fid => $file) {
      // Delete all files associated with the node
      $sql = 'DELETE FROM {files} WHERE fid = %d';
      db_query(db_rewrite_sql($sql), $fid);
      file_delete($file->filepath);

      // Delete all file revision information associated with the node
      $sql = 'DELETE FROM {upload} WHERE fid = %d';
      db_query(db_rewrite_sql($sql), $file->fid);
    }
  }
}

/**
 * Implementation of hook_menu().
 */
function attachment_expiration_menu() {
  $items['admin/settings/attachment_expiration'] = array(
    'title' => t('Attachment expiration'),
    'description' => t('Set a expiration date to delete old attachments.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_attachment_expiration_settings'),
    'access arguments' => array('attachment expiration'),
  );
  return $items;
}

/**
 * Builds array for the for module setting's arguments.
 *
 * @return
 *   Array of node nid's.
 */
function _attachment_expiration_settings() {
  $form['attachment_expiration'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#description' => t('Limit the lifespan of attachments by choosing the options below.'),
    '#collapsible' => TRUE,
  );

  $form['attachment_expiration']['attachment_exp_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => variable_get('attachment_exp_active', ''),
    '#description' => t('Turn on or off functionality.'),
  );

  $form['attachment_expiration']['attachment_exp_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiration'),
    '#default_value' => variable_get('attachment_exp_date', ''),
    '#description' => t('Select the amount of time for attachments to live in days.'),
    '#required' => TRUE,
  );

  // Pull back node types and create checkboxes
  $options = node_get_types('names');

  $form['attachment_expiration']['attachment_exp_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Type'),
    '#default_value' => variable_get('attachment_exp_type', ''),
    '#description' => t('Choose which node types this option should apply to.'),
    '#options' => $options,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
